Mongo Connector
===
This repository contains a package named storage that allows you to connect to a MongoDB database with an example main file that is using it.

## Configuration

### What do you need 
- Golang
- MongoDB 
- Golang dependency manager

### Optional
To use the `storage` package, you can use a configuration json file like follows
```json
{
    "connectionstring": "mongodb://localhost/testdb",
    "ttl": "720h"
}
```
You can also add other optional parameters like :
- Timeout(default 15s) : `timeout`
- Consistency(default monotonic) : `consistency`
- PoolSize(default 30) : `poolsize`
If you don't want to use a json configuration file you can pass an object to the `storage.NewMongoConfiguration` func

If you need more parameters, you can add more fields on the `storage.MongoConfiguration` type by overriding the struct like follows :
```golang
type MongoConfiguration struct {
    storage.MongoConfiguration
    CustomField   string
}
```

## Example usage
Here is an example, using the package by reading a configuration json file and passing the fields to create a configuration and connect to a database :
```golang
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/nirmou/mongo/storage"
)

func main() {
	// Using config file ./config.json
	file, err := ioutil.ReadFile("config.json")
	if err != nil {
		fmt.Printf("File error : %v \n", err)
		os.Exit(1)
	}
	var config interface{}
	json.Unmarshal(file, &config)

	// Configuration Mongo
	c, err := storage.NewMongoConfiguration(config)
	if err != nil {
		fmt.Printf("Configuration settings error : %v \n", err)
		os.Exit(1)
	}

	// Reaching server
	mConnector := storage.MongoConnector{}
	err = mConnector.Configure(c)
	fmt.Printf("Connector %v \n", mConnector)
	if err != nil {
		fmt.Printf("Configuration error %v \n", err)
		os.Exit(1)
	}

	// Copying session for performing request to database
	session := mConnector.GetSession()
	fmt.Printf("Mongo Session %v \n", session)
}
```

After getting the session you can start performing request within your database :
```golang
c := session.DB(database).C(collection)
err := c.Find(query).One(&result)
```
### Run this example 
If you want to run this example, just clone this repository and download dependencies with the `dep` command and run it 
```
$ > dep ensure
$ > go run main.go
```

## References 
- Godep : https://github.com/golang/dep
- I have used the package mgo for write this code which is available here : https://godoc.org/gopkg.in/mgo.v2
