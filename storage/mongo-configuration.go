package storage

// MongoConfiguration contains parameters for a mongo configuration
import (
	"encoding/json"
	"regexp"
	"strconv"
	"strings"
)

// MongoConfiguration type for containing MongoDB settings
type MongoConfiguration struct {
	ConnectionString *string `json:"connectionstring" valid:"required"`
	Timeout          *string `json:"timeout"`
	Consistency      *string `json:"consistency"`
	PoolSize         *int    `json:"poolsize"`
}

// NewMongoConfiguration returns a MongConfiguration type from a json config file
func NewMongoConfiguration(config interface{}) (*MongoConfiguration, error) {
	cf := &MongoConfiguration{}
	cba, err := json.Marshal(config)
	if err != nil {
		return nil, err
	}

	json.Unmarshal(cba, cf)
	if cf.Timeout == nil {
		t := defaultTimeout
		cf.Timeout = &t
	}
	if cf.Consistency == nil {
		c := defaultConsistency
		cf.Consistency = &c
	}
	if cf.PoolSize == nil {
		p := defaultPoolsize
		cf.PoolSize = &p
	}

	return cf, nil
}

// String get the current MongoConfiguration instance to a list of a string
func (mc *MongoConfiguration) String() string {
	r, _ := regexp.Compile(DatabaseURIRegexp)
	pwd := r.ReplaceAllString(*mc.ConnectionString, DatabaseURIReplacement)
	s := []string{pwd, *mc.Timeout, *mc.Consistency, strconv.Itoa(*mc.PoolSize)}
	return strings.Join(s, " ")
}
