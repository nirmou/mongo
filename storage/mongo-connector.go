package storage

import (
	"time"

	"github.com/sirupsen/logrus"
	mgo "gopkg.in/mgo.v2"
)

const (
	// default configuration values for non mandatory fields
	defaultTimeout     = "15s"
	defaultConsistency = "monotonic"
	defaultPoolsize    = 30
	// DatabaseURIRegexp regexp for database string connection
	DatabaseURIRegexp = "(\\w+:)//(?:([^:]+)(?::(.*))@)?([^/]+)(?:/(\\w+))?"
	// DatabaseURIReplacement string for replacement of database string connection parameters
	DatabaseURIReplacement = "$1//****:****@$4/$5"
)

// MongoConnector type for creating an instance to connect to the database
type MongoConnector struct {
	session       *mgo.Session
	configuration *MongoConfiguration
}

// Configure set the parameters for MongoDB database connection
func (mc *MongoConnector) Configure(config *MongoConfiguration) error {
	mc.configuration = config

	d, err := time.ParseDuration(*mc.configuration.Timeout)
	if err != nil {
		return err
	}

	// mongo connection
	mgoSession, err := mgo.DialWithTimeout(*mc.configuration.ConnectionString, d)
	if err != nil {
		return err
	}

	// set 30 sec timeout on session
	mgoSession.SetSyncTimeout(d)
	mgoSession.SetSocketTimeout(d)

	var m mgo.Mode
	// set mode
	switch *mc.configuration.Consistency {
	case "eventual":
		m = mgo.Eventual
	case "strong":
		m = mgo.Strong
	default:
		m = mgo.Monotonic
	}

	mgoSession.SetMode(m, true)
	mgoSession.SetPoolLimit(*mc.configuration.PoolSize)
	mc.session = mgoSession

	return nil
}

// GetSession copy the session of the MongoDB instance for database operation
func (mc *MongoConnector) GetSession() *mgo.Session {
	if mc.session == nil {
		logrus.Fatal("Session does not existing, call MongoConnector.Configure() first.")
	}
	return mc.session.Copy()
}
