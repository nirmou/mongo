package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/nirmou/mongo/storage"
)

func main() {
	// Using config file ./config.json
	file, err := ioutil.ReadFile("config.json")
	if err != nil {
		fmt.Printf("File error : %v \n", err)
		os.Exit(1)
	}
	var config interface{}
	json.Unmarshal(file, &config)

	// Configuration Mongo
	c, err := storage.NewMongoConfiguration(config)
	if err != nil {
		fmt.Printf("Configuration settings error : %v \n", err)
		os.Exit(1)
	}

	// Reaching server
	mConnector := storage.MongoConnector{}
	err = mConnector.Configure(c)
	fmt.Printf("Connector %v \n", mConnector)
	if err != nil {
		fmt.Printf("Configuration error %v \n", err)
		os.Exit(1)
	}

	// Copying session for performing request to database
	session := mConnector.GetSession()
	fmt.Printf("Mongo Session %v \n", session)
}
